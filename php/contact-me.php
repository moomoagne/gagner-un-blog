<?php
if($_POST) {

//$from = array('contact@qualshore.com' =>'QUALSHORE');
/*    $to_Email = array(
        'contact@qualshore.com'
        'qsfrance@qualshore.com',
        'mamadou.agne@qualshore.com'
    );*/

    $to_Email = 'mamadou.agne@qualshore.com'; // Write your email here to receive the form submissions
    $subject = 'Nouveau visiteur'; // Write the subject you'll see in your inbox
   
    // Use PHP To Detect An Ajax Request
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
   
        // Exit script for the JSON data
        $output = json_encode(
        array(
            'type'=> 'error',
            'text' => 'Request must come from Ajax'
        ));
       
        die($output);
    }
   
    // Checking if the $_POST vars well provided, Exit if there is one missing
    if(!isset($_POST["userName"]) || !isset($_POST["userEmail"]) || !isset($_POST["userTelephone"]) || !isset($_POST["userMessage"])) {
        
        $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> Les champs de saisie sont vides!'));
        die($output);
    }
   
    // PHP validation for the fields required
    if(empty($_POST["userName"])) {
        $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> Nous sommes désolés mais votre nom est trop court ou non spécifié.'));
        die($output);
    }
    
    if(!filter_var($_POST["userEmail"], FILTER_VALIDATE_EMAIL)) {
        $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> S\'il vous plaît, mettez une adresse email valide.'));
        die($output);
    }

    // To avoid the spammy bots, you can change the value of the minimum characters required. Here it's <20
    if(strlen($_POST["userMessage"])<20) {
        $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> Message trop court! Prenez votre temps et écrivez quelques mots.'));
        die($output);
    }
   
  // Proceed with PHP email
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";
    $headers .= 'From: LEGEND Template <noreply@yourdomain.com>' . "\r\n"; // As an example, the 'From' address should be set to something like 'noreply@yourdomain.com' in order to be based on the same domain as the form.
    $headers .= 'Reply-To: '.$_POST["userEmail"]."\r\n";
    

    /*
       'X-Mailer: PHP/' . phpversion();
   // Body of the Email received in your Mailbox
   $emailcontent = 'Hey! You have received a new message from the visitor <strong>'.$_POST["userName"].'</strong><br/><br/>'. "\r\n" .
               'His message: <br/> <em>'.$_POST["userMessage"].'</em><br/><br/>'. "\r\n" .
               '<strong>Feel free to contact '.$_POST["userName"].' via email at : '.$_POST["userEmail"].'</strong>' . "\r\n" ;

   $Mailsending = @mail($to_Email, $subject, $emailcontent, $headers);

   if(!$Mailsending) {

       //If mail couldn't be sent output error. Check your PHP email configuration (if it ever happens)
       $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> Oops! Looks like something went wrong, please check your PHP mail configuration.'));
       die($output);

   } else {
       $output = json_encode(array('type'=>'message', 'text' => '<i class="icon ion-checkmark-round"></i> Hello '.$_POST["userName"] .', Your message has been sent, we will get back to you asap !'));
       die($output);
   }*/


    // load composer packages
    require_once '../swiftmailer/swiftmailer/lib/swift_required.php';


    $corps= $_POST['userMessage'];
    $source=$_POST['userEmail'];
    $emailcontent = '<p>Salut! Vous avez reçu un nouveau message du visiteur <strong>'.$_POST["userName"].'</strong></p><br/>'. "\r\n" .
        '<p>Téléphone : '.$_POST["userTelephone"].'</p>' . "\r\n".
        '<p>Message : '.$_POST["userMessage"].'</p><br/><br/>'. "\r\n" .
        '<p>N\'hésitez pas à contacter '.$_POST["userName"].' par email '.$_POST["userEmail"].'</p>' ."\r\n".
        'Merci'
    ;
    $transport = Swift_SmtpTransport::newInstance('ssl0.ovh.net', 587);
    $transport->setUsername($to_Email);
    $transport->setPassword('Mamadou2016');

    $swift = Swift_Mailer::newInstance($transport);
    $message = new Swift_Message("Blog | Qualshore ");
    $message->setFrom(array($source => $source));
    $aMailsDest = array('contact@qualshore.com'=>'Qualshore Dakar','qsfrance@qualshore.com'=>'Qualshore Paris','mamadou.agne@qualshore.com'=>'Mamadou AGNE');
    //$message->setBody('Salut! Vous avez reçu un nouveau message du visiteur \n Prénom & Nom : '.$_POST["userName"].'\n Télephne : '.$_POST["userTelephone"].'');
    $message->setTo($aMailsDest);
    //$message->setTo(['contact@qualshore.com','qsfrance@qualshore.com','mamadou.agne@qualshore.com'=>'Qualshore']);
    $message->setBody($emailcontent, 'text/html');

    if($swift->send($message)){
        //echo "send";
        $output = json_encode(array('type'=>'message', 'text' => '<i class="icon ion-checkmark-round"></i> Bonjour '.$_POST["userName"] .', Votre message a été envoyé, nous vous répondrons dès que possible !'));
        die($output);
    }
    else{
       // echo "error";
        $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> Oops! erreur lors de l\'envoie'));
        die($output);
    }

    /*header('Location:index.php');*/

}
?>